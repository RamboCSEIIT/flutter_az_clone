import 'package:amazon_clone/utils/color_themes.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const AmazoneClone());
}

class AmazoneClone extends StatelessWidget {
  const AmazoneClone({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Amazon Clone',
      debugShowCheckedModeBanner: false,
      theme:
          ThemeData.light().copyWith(scaffoldBackgroundColor: backgroundColor),
      home: Scaffold(
        appBar: AppBar(title: const Text('Testing Clone')),
        body: const Text("Scafold Welcome"),
      ),
    );
  }
}
